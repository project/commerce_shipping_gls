INTRODUCTION
------------
This project provides <a href="http://www.gls.dk">GSL parcel shop shipping
service</a> integration as new shipping method for Drupal commerce.
It uses GLS wsPakkeshop webservice to get data about GLS parcel shops locations
and display them to end user as possible delivery points.
You can set shipping rate amount in
<a href="admin/commerce/config/shipping/methods/gls-shipping-method/settings">
Commerce shipping GLS settings</a>


REQUIREMENTS
------------
This module requires the following module:
 * Drupal Commerce (https://www.drupal.org/project/commerce)
 * Commerce Shipping (https://www.drupal.org/project/commerce_shipping)


 INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


MAINTAINERS
-----------
Current maintainers:
 * Irmantas P. (irmantasp) - https://www.drupal.org/u/irmantasp

This project has been sponsored by:
 * Adapt A/S - https://www.drupal.org/node/1897408
